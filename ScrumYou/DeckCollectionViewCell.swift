//
//  DeckCollectionViewCell.swift
//  ScrumYou
//
//  Created by Helder Nunes on 31/05/2018.
//  Copyright © 2018 Helder Nunes. All rights reserved.
//

import UIKit

class DeckCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var cardImageView: UIImageView!
}
