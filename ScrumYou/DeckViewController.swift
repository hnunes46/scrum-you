//
//  DeckViewController.swift
//  ScrumYou
//
//  Created by Helder Nunes on 31/05/2018.
//  Copyright © 2018 Helder Nunes. All rights reserved.
//

import UIKit

class DeckViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var deckCollectionView: UICollectionView!
    let deckValues: [[String : String]] = [
        ["cardValue": "0", "cardImage": "monkey", "cardDescription": "Hire a monkey"],
        ["cardValue": "1/2", "cardImage": "piece-of-cake", "cardDescription": "Piece of cake"],
        ["cardValue": "1", "cardImage": "laughing", "cardDescription": "It's all you got?"],
        ["cardValue": "2", "cardImage": "bored", "cardDescription": "BORING!!!"],
        ["cardValue": "3", "cardImage": "ninja", "cardDescription": "Ninja Style"],
        ["cardValue": "5", "cardImage": "god", "cardDescription": "God Mode!"],
        ["cardValue": "8", "cardImage": "pinocchio", "cardDescription": "You know will be 10!"],
        ["cardValue": "13", "cardImage": "split", "cardDescription": "You know you should split this!"],
        ["cardValue": "infinity", "cardImage": "gravestone", "cardDescription": "Keep dreaming..."],
        ["cardValue": "?", "cardImage": "confused", "cardDescription": "What???"],
        ["cardValue": "coffe", "cardImage": "sleepy", "cardDescription": "Zzzzzz...zzzz"]
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        deckCollectionView.delegate = self
        deckCollectionView.dataSource = self
        
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deckValues.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "deckCell", for: indexPath) as! DeckCollectionViewCell
        
        if deckValues[indexPath.row]["cardValue"] == "infinity" {
            cell.cardImageView.isHidden = false
            cell.cardLabel.isHidden = true
            cell.cardImageView.image = UIImage(named: "infinity")
        } else if deckValues[indexPath.row]["cardValue"] == "coffe" {
            cell.cardImageView.isHidden = false
            cell.cardLabel.isHidden = true
            cell.cardImageView.image = UIImage(named: "coffe")
        } else {
            cell.cardImageView.isHidden = true
            cell.cardLabel.isHidden = false
            cell.cardLabel.text = deckValues[indexPath.row]["cardValue"]
        }
        
        
        cell.cardLabel.text = deckValues[indexPath.row]["cardValue"]
        
        cell.layer.cornerRadius = 5.0
        cell.layer.masksToBounds = true
        
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 5.0
        cell.layer.shadowOpacity = 0.5
        cell.layer.masksToBounds = false
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cardViewController = self.storyboard?.instantiateViewController(withIdentifier: "cardVC") as! CardViewController
        cardViewController.cardValue = deckValues[indexPath.row]["cardValue"]
        cardViewController.cardImageValue = deckValues[indexPath.row]["cardImage"]
        cardViewController.cardDescriptionValue = deckValues[indexPath.row]["cardDescription"]
        navigationController?.pushViewController(cardViewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: deckCollectionView.frame.width / 3 - 10, height: deckCollectionView.frame.height / 4 - 10)
    }
}
