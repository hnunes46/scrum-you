//
//  CardViewController.swift
//  ScrumYou
//
//  Created by Helder Nunes on 31/05/2018.
//  Copyright © 2018 Helder Nunes. All rights reserved.
//

import UIKit

class CardViewController: UIViewController {
    
    @IBOutlet weak var cardContentView: UIView!
    @IBOutlet weak var cardButton: UIButton!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var cardDescription: UILabel!
    
    var isOpen: Bool = false
    var cardValue: String!
    var cardImageValue: String!
    var cardDescriptionValue: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardContentView.layer.cornerRadius = 5.0
        cardContentView.layer.masksToBounds = true
        
        cardContentView.layer.shadowColor = UIColor.black.cgColor
        cardContentView.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cardContentView.layer.shadowRadius = 5.0
        cardContentView.layer.shadowOpacity = 0.5
        cardContentView.layer.masksToBounds = false
        
        cardButton.setTitle("ScrumYou", for: .normal)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            flipCard()
        }
    }
    
    @IBAction func cardTapped(_ sender: Any) {
        flipCard()
    }
    
    func flipCard () {
        if isOpen {
            isOpen = false
            cardButton.titleLabel?.font = UIFont(name: "Noteworthy-Bold", size: 30)
            cardButton.setTitle("SrumYou", for: .normal)
            cardButton.setImage(UIImage(), for: .normal)
            cardButton.contentVerticalAlignment = .center
            cardDescription.text = ""
            cardImage.image = nil
            UIView.transition(with: cardContentView, duration: 0.3, options: .transitionFlipFromLeft, animations: nil, completion: nil)
        } else {
            isOpen = true
            if cardValue == "coffe" {
                cardButton.setTitle("", for: .normal)
                cardButton.setImage(UIImage(named: "coffe"), for: .normal)
                cardButton.tintColor = .white
                
            } else if cardValue == "infinity" {
                cardButton.setTitle("", for: .normal)
                cardButton.setImage(UIImage(named: "infinity"), for: .normal)
                cardButton.tintColor = .white
            } else {
                cardButton.titleLabel?.font = UIFont(name: "Noteworthy-Bold", size: 85)
                cardButton.setTitle(cardValue, for: .normal)
            }
            cardButton.contentVerticalAlignment = .top
            cardImage.image = UIImage(named: cardImageValue)
            cardDescription.text = cardDescriptionValue
            UIView.transition(with: cardContentView, duration: 0.3, options: .transitionFlipFromRight, animations: nil, completion: nil)
        }
    }
}
